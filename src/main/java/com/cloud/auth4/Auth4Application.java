package com.cloud.auth4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
public class Auth4Application {

    public static void main(String[] args) {
        SpringApplication.run(Auth4Application.class, args);
    }
}
